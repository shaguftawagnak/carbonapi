<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarbondetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carbondetails', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->text('activity');
            $table->text('activitytype');
            $table->string('fueltype');
            $table->string('country');
            $table->string('mode')->nullable();
            $table->float('footprint');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carbondetails');
    }
}
