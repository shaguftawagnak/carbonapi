<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carbondetail extends Model
{
    protected $fillable = ["activity","activitytype","fueltype","country", "mode", "footprint"]; 
}
