<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Http\Resources\Carbondata;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Carbondetail;


class CarbonController extends Controller
{
    public function getFootprintResult(Request $request){
        
        $activity = $request->route('activity');
        $activitytype = $request->route('activitytype');
        $country = $request->route('country');
        $fueltype = $request->route('fueltype');

        if(!is_numeric($activity) || $activity == 0) {
            return "Activity should be number and not equal to 0";
        }

        //*** mandatory condition check for mode param when activity type is miles and also setting optional parameter of the same in api */
        $mode = null;
        $optionalparam='';
        if( $request->route('mode') != null) {

            $mode = $request->route('mode');
            $optionalparam.="&mode=".$mode;
        
        } else if($activitytype == 'miles') {

            return "Mode is compulsory for miles ActivityType";
        }


       
        if(is_numeric($activitytype) || is_numeric($country) || is_numeric($fueltype) || ( ($mode != null) &&  is_numeric($mode) ) ) {
            return "Either Activity Type, Country, Fuel Type or Mode parameter is in number format";
        }



        //*** regex applied to check if any parameter contains special characters or combination of string and numbers */ 
        if ( (preg_grep('/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/', [$fueltype,$activitytype,$country,$mode])) && (preg_grep("/^[a-z A-Z0-9\\/\\\\.'\"]+$/", [$fueltype,$activitytype,$country,$mode])) ) {

            return "Activity Type, Country, Fuel Type and Mode parameter should be only in string format";
        }



        if(empty(Cache::get('footprintresult'))) {
        // if(1) {
            
            $client = new Client(); 
            try {
 
                $carbonResult = $client->request('GET', "https://api.triptocarbon.xyz/v1/footprint?activity=".$activity."&activityType=".$activitytype."&country=".$country."&fuelType=".$fueltype.$optionalparam);
                $statusCode = $carbonResult->getBody()->getContents();
               
            } catch( \Exception $e ) {

                return "Incorrect details Entered";
            }
            
            $footprint =  new Carbondata(json_decode($statusCode));
            Cache::put('footprintresult', $footprint, 86400);
            
            $carbonval = Carbondetail::create([
                'activity' => $activity,
                'activitytype' => $activitytype,
                'country' => $country,
                'fueltype' => $fueltype,
                'mode'=>$mode,
                'footprint' => $footprint->resource->carbonFootprint,
            ]);
           
        } 
     

        $value = Cache::get('footprintresult');
        return "carbon Footprint: ".$value->carbonFootprint;
      
    }

   
}
